﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using TwilightSparkle.Common.Hasher;
using TwilightSparkle.Common.Services;
using TwilightSparkle.Forum.DomainModel.Entities;
using TwilightSparkle.Forum.Repository.Interfaces;

namespace TwilightSparkle.Forum.Foundation.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private const string UsernamePattern = @"^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$";
        private const string PasswordPattern = @"(?=^.{8,}$)(?=.*\d)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";
        private const string EmailPattern = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";

        private static readonly Regex UsernameRegex;
        private static readonly Regex PasswordRegex;
        private static readonly Regex EmailRegex;


        private readonly IHasher _hasher;
        private readonly IForumUnitOfWork _unitOfWork;

        private readonly ILogger<AuthenticationService> _logger;


        public AuthenticationService(IHasher hasher, IForumUnitOfWork unitOfWork, ILogger<AuthenticationService> logger)
        {
            _hasher = hasher;
            _unitOfWork = unitOfWork;

            _logger = logger;
        }

        static AuthenticationService()
        {
            UsernameRegex = new Regex(UsernamePattern);
            PasswordRegex = new Regex(PasswordPattern);
            EmailRegex = new Regex(EmailPattern);
        }


        public async Task<ServiceResult<SignUpErrorType>> SignUpAsync(SignUpDto signUpDto)
        {
            _logger.LogDebug($"Trying to sign up user: username - {signUpDto.Username}, email - {signUpDto.Email}");
            var isValidUsername = CheckIfValidUsername(signUpDto.Username);
            if (!isValidUsername)
            {
                _logger.LogTrace($"Username {signUpDto.Username} is invalid");

                return ServiceResult.CreateFailed(SignUpErrorType.InvalidUsername);
            }
            _logger.LogTrace($"Username {signUpDto.Username} is valid");

            var isValidPassword = CheckIfValidPassword(signUpDto.Password);
            if (!isValidPassword)
            {
                _logger.LogTrace($"Password for user with username {signUpDto.Username} is invalid");

                return ServiceResult.CreateFailed(SignUpErrorType.InvalidPassword);
            }
            _logger.LogTrace($"Password for user with username {signUpDto.Username} is valid");

            var isValidPasswordConfirmation = signUpDto.PasswordConfirmation == signUpDto.Password;
            if (!isValidPasswordConfirmation)
            {
                _logger.LogTrace($"Password confirmation for user with username {signUpDto.Username} is invalid");

                return ServiceResult.CreateFailed(SignUpErrorType.PasswordAndConfirmationNotSame);
            }
            _logger.LogTrace($"Password confirmation for user with username {signUpDto.Username} is valid");

            var isValidEmail = CheckIfValidEmail(signUpDto.Email);
            if (!isValidEmail)
            {
                _logger.LogWarning($"Unsuccessful attempt to sign up user: email {signUpDto.Email} is invalid");

                return ServiceResult.CreateFailed(SignUpErrorType.InvalidEmail);
            }
            _logger.LogTrace($"Email {signUpDto.Email} for user with username {signUpDto.Username} is valid");


            var userRepository = _unitOfWork.UserRepository;
            var duplicateUser = await userRepository.GetFirstOrDefaultAsync(u => u.Username == signUpDto.Username);
            if (duplicateUser != null)
            {
                _logger.LogWarning($"Unsuccessful attempt to sign up user: username {signUpDto.Username} is already taken");

                return ServiceResult.CreateFailed(SignUpErrorType.DuplicateUsername);
            }
            _logger.LogTrace($"No duplicates for username {signUpDto.Username}");

            duplicateUser = await userRepository.GetFirstOrDefaultAsync(u => u.Email == signUpDto.Email);
            if (duplicateUser != null)
            {
                _logger.LogWarning($"Unsuccessful attempt to sign up user: email {signUpDto.Email} is already in use");

                return ServiceResult.CreateFailed(SignUpErrorType.DuplicateEmail);
            }
            _logger.LogTrace($"No duplicates for email {signUpDto.Email}; username - {signUpDto.Username}");
            _logger.LogDebug($"User data passed validations: username - {signUpDto.Username}, email - {signUpDto.Email}");


            var passwordHash = _hasher.GetHash(signUpDto.Password);
            var newUser = new User
            {
                Username = signUpDto.Username,
                Email = signUpDto.Email,
                PasswordHash = passwordHash,
                ProfileImageId = null
            };
            userRepository.Create(newUser);
            try
            {
                _logger.LogTrace($"Saving user to database with username - {signUpDto.Username}");

                await _unitOfWork.SaveAsync();
            }
            catch(Exception exception)
            {
                _logger.LogError($"Unsuccessful attempt to save signed up user {exception}");

                return ServiceResult.CreateFailed(SignUpErrorType.Unknown);
            }

            _logger.LogInformation($"Created user: username - {newUser.Username}");

            return ServiceResult<SignUpErrorType>.CreateSuccess();
        }

        public async Task<ServiceResult<SignInErrorType>> SignInAsync(string username, string password, bool rememberMe, SignInHandler signInHandler)
        {
            _logger.LogDebug($"Trying to sign in user: username - {username}");
            var userRepository = _unitOfWork.UserRepository;
            var passwordHash = _hasher.GetHash(password);
            var user = await userRepository.GetFirstOrDefaultAsync(u => u.Username == username && u.PasswordHash == passwordHash);
            if(user == null)
            {
                _logger.LogTrace($"Invalid credentials for user with username {username}");

                return ServiceResult.CreateFailed(SignInErrorType.InvalidCredentials);
            }

            await signInHandler(user.Username, user.Id, rememberMe);

            _logger.LogInformation($"User signed in: username - {user.Username}");

            return ServiceResult<SignInErrorType>.CreateSuccess();
        }

        public async Task SignOutAsync(SignOutHandler signOutHandler)
        {
            await signOutHandler();
        }


        private static bool CheckIfValidUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return false;
            }

            var isUsernameValid = UsernameRegex.IsMatch(username);

            return isUsernameValid;
        }

        private static bool CheckIfValidPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                return false;
            }

            var isPasswordValid = PasswordRegex.IsMatch(password);

            return isPasswordValid;
        }

        private static bool CheckIfValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            var isEmailValid = EmailRegex.IsMatch(email);

            return isEmailValid;
        }
    }
}